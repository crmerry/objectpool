#pragma once

/* ***************************************
[메모리 풀 개요]
heap 공간에 메모리를 자주 할당 하고 해제하는 일을 반복적으로 하는 일들을 할 때,
속도 향상 및 메모리 단편화 방지를 위하여 사용한다.
또한, 프로그램에서 사용할 메모리를 미리 확보해둠으로써,
메모리 사용에 대한 추적 등의 추가적인 기능도 꾀할 수 있다.

기본적인 원리는 할당을 할 때 사용하는 연산 및 함수인
new, malloc, delete, free를 두 번 호출하지 않도록 하는 것이다.
최초에 사용할 메모리를 확보할 때 new를 한 번 하고, 프로그램 종료시에 delete를 하는 것이 끝이며,
중간에 메모리를 할당 할 때는, 이미 확보한 메모리에서 일정 부분을 주는 것이다.

[세부사항 1]
생성자 및 가상함수 테이블을 세팅해줘야 한다.
new는 세 가지 일을 담당한다
1. 운영체제로부터 메모리를 할당받음.
2. 생성자를 호출함
3. 가상함수를 세팅.

2와 3의 기능을 제공해주는 방법이 필요.
: placement new : new (&변수) 타입
을 사용하면, 변수에 해당하는 메모리를 그대로 사용 하면서
타입에 해당하는 생성자 호출 및 가상함수를 세팅해준다.

[세부사항 2]
MemoryLeakChecer와 같은 new 오버로딩한 경우를 대비하여,
ALLOC에서 placement new는 그대로 사용할 수 있도록 전처리.

[세부사항 3]
메모리 해제
1. 메모리 풀에 속한 모든 메모리(현재 사용가능한 메모리 및 이미 할당한 메모리 모두 포함)를
메모리 풀에서 모두 해제
2. 메모리 풀에 속한 현재 사용가능한 메모리만을 해제 하는 경우

1을 달성하려면, 이미 할당한 메모리에 대한 정보를 저장(할당할 때마다)하여, 소멸자에서 해제 해줘야 하고
2의 경우라면, 무시해도 된다. 즉, 사용자의 실수로 인한 메모리 릭으로 여기는 것.
현재는 2의 방식.

[세부사항 4]
현재 구성된 메모리 풀은 메모리 풀이라기 보다는 오브젝트 풀에 가깝다.
따라서, 같은 타입의 오브젝트를
다루기 때문에, 메모리 해제 시에 메모리 풀과 같은 이슈가 없다.
메모리 풀에서의 메모리 이슈는
char buf[1024]와 같은 형태의 메모리에서 obj1, obj2...와 같은 형식으로 할당 했을 때(placement new를 이용하여),
메모리 해제를 어떤 방식으로 하냐는 것이다.
즉, 곧바로 delete[] buf를 해야 하느냐?
아니면 obj1, obj2에 각각에 메모리 해제를 해야 하느냐?

적절한 방법은 할당한 obj1, obj2에 대하여 모두 소멸자를 호출한 후,
free(buf)를 통하여 메모리 삭제를 해야하는 것이다.

delete의 경우 메모리 해제 및 소멸자 호출이 있는데, placement new를 통하여 메모리를 떼어주면,
delete[] buf를 하더라도 소멸자 호출이 제대로 이루어지지 않는다.
따라서, 할당받은 각각의 오브젝트들이 manually 하게 소멸자를 호출해준후,
free(buf)로 메모리를 해제하는것이다.

[세부사항 5]
1> b_placement_new

b_placement_new가 true이면
MemoryPool의 생성자에서 new가 아닌 malloc을 호출해주며\
Alloc을 호출했을 때, placement new를 호출해준다.
Free를 호출했을 때, delete를 호출해준다.
그리고 소멸자에서, free를 호출해준다.

만약 MemoryPool의 생성자에서 new가 호출되었으면, 생성자 호출이 두 번 된 셈이라 메모리 누수가 발생할 것.

현재 코드를 살펴보면, b_placement_new가 true인 경우,  Alloc을 호출할 마다, 생성자가 계속 호출 된다.

b_placement_new가 false이면
MemoryPool의 생성자에서 new를 호출해주며
Alloc을 호출했을 때, 그대로 전해주고
Free를 호출했을 , 그대로 받아준다.
그리고 소멸자에서, delete를 호출해준다.

2> 올바른 블럭 확인

제대로된 블럭인지 확인 하는 _code는 1바이트가 아니라 4바이트나 8바이트로 잡고 숫자도 크게 잡아준다.
어차피 비트 패딩에 의하여 4나 8바이트로 잡히게 됨.
**************************************** */

/* ***************************************
헤더
**************************************** */
#include <Windows.h>
#include <assert.h>
#include <new.h>
#include <stddef.h>
#include <stdlib.h>

namespace memorypool
{
	template <typename DATA>
	class MemoryPool
	{
	public:
		/* ***************************************
		number_of_blocks
		: 0인 경우, 프리 리스트 처럼 작동
		**************************************** */
		MemoryPool(int number_of_blocks, bool b_placement_new = false);
		virtual	~MemoryPool();

		/* ***************************************
		하나의 블럭을 할당, 블럭 포인터 리턴
		**************************************** */
		DATA*	Alloc();

		/* ***************************************
		하나의 블럭을 해제, 성공 실패 리턴
		**************************************** */
		bool	Free(DATA *pData);

		/* ***************************************
		총 할당된 블럭의 수를 반환.
		**************************************** */
		int		GetCapacity() const;

		/* ***************************************
		할당되어 프로그램에서 사용되고 있는 오브젝트의 수
		**************************************** */
		int		GetSize() const;

	private:
		struct Block
		{
			enum
			{
				Code = 0x11223344,
			};

			long long	_code = Code;
			Block*		_next = nullptr;
			DATA		_data;
		};

		Block*	_top;
		int		_capacity;
		int		_size;
		bool	_b_placement_new;
		CRITICAL_SECTION _cs;
	};

	template<typename DATA>
	MemoryPool<DATA>::MemoryPool(int number_of_blocks, bool b_placement_new)
		:_top(nullptr), _capacity(number_of_blocks), _size(0), _b_placement_new(b_placement_new)
	{
		Block* object;

		if (b_placement_new)
		{
			while (number_of_blocks-- > 0)
			{
				object = (Block*)malloc(sizeof(Block));

				object->_code = Block::Code;
				object->_next = _top;
				_top = object;
			}
		}
		else
		{
			while (number_of_blocks-- > 0)
			{
				object = new Block;

				object->_next = _top;
				_top = object;
			}
		}

		InitializeCriticalSection(&_cs);
	}

	template<typename DATA>
	MemoryPool<DATA>::~MemoryPool()
	{
		Block* next;

		if (_b_placement_new)
		{
			while (_top != nullptr)
			{
				next = _top->_next;
				free(_top);
				_top = next;
			}
		}
		else
		{
			while (_top != nullptr)
			{
				next = _top->_next;
				delete _top;
				_top = next;
			}
		}

		DeleteCriticalSection(&_cs);
		/* ***************************************
		TODO: capacity(즉 오브젝트 풀에 할당된 총 오브젝트의 수에 대한 정보가 필요하다면...
		이곳에서 로그를 남겨주는 것이 좋다)
		**************************************** */
	}

	template<typename DATA>
	DATA* MemoryPool<DATA>::Alloc()
	{
		/* ***************************************
		_top이 nullptr인 경우는 두 가지.

		1. 메모리풀에 할당된(number_of_objects)수가 모두 소진
		2. FreeList형식으로 할당하는 경우

		2번의 경우 바로 메모리를 할당하여 돌려주면 됨.
		1의 경우는
		1) 메모리를 할당하여 돌려줌
		2) nullptr을 돌려줌.

		그런데, nullptr을 돌려줘서 새로운 오브젝트를 만들지 못하게 된다면... 여러 문제가 터질 것.
		따라서 1과 2의 방식 모두 메모리를 새로 할당하여 돌려주는 방식으로 사용.
		**************************************** */
		EnterCriticalSection(&_cs);

		if (_top == nullptr)
		{
			Block* block;

			if (_b_placement_new)
			{
				block = (Block*)malloc(sizeof(Block));

				block->_code = Block::Code;

				/* ***************************************
				Memory Leak Checker와 함께 사용할 경우
				충돌 방지용.
				**************************************** */
				#ifndef  __MEMORY_LEAK_CHECKER__			
					new (&block->_data) DATA;
				#else
					#undef new
					new (&block->_data) DATA;
					#define new new(__FILE__, __LINE__)
				#endif
			}
			else
			{
				block = new Block;
			}

			_capacity++;
			_size++;

			LeaveCriticalSection(&_cs);

			return &block->_data;
		}

		DATA* target = &_top->_data;
		_top = _top->_next;
		_size++;

		LeaveCriticalSection(&_cs);
		/* ***************************************
		생성자 호출
		**************************************** */
		if (_b_placement_new)
		{
			#ifndef  __MEMORY_LEAK_CHECKER__			
				new (target) DATA;
			#else
				#undef new
				new (target) DATA;
				#define new new(__FILE__, __LINE__)
			#endif
		}

		return target;
	}

	template<typename DATA>
	bool MemoryPool<DATA>::Free(DATA* data)
	{
		EnterCriticalSection(&_cs);

		Block* block = (Block*)((char*)data - offsetof(Block, _data));

		/* ***************************************
		잘못된 값이 올 경우를 확인.
		**************************************** */
		assert(block->_code == Block::Code);
		if (block->_code != Block::Code)
		{
			LeaveCriticalSection(&_cs);

			return false;
		}

		block->_next = _top;
		_top = block;
		_size--;

		LeaveCriticalSection(&_cs);

		if (_b_placement_new)
		{
			data->~DATA();
		}

		return true;
	}

	template<typename DATA>
	int MemoryPool<DATA>::GetCapacity() const
	{
		return _capacity;
	}

	template<typename DATA>
	int MemoryPool<DATA>::GetSize() const
	{
		return _size;
	}
}