#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <Windows.h>

#include "my_new.h"
#include "MemoryPool.h"
#include "Profiler.h"

class Widget
{
public:
	Widget();
	~Widget();
	int a;
private:

};

Widget::Widget()
{
	printf("Widget Constructor \n");
}

Widget::~Widget()
{
	printf("Widget Destructor \n");
}
class Base
{
public:
	Base();
	virtual ~Base();
	virtual void Test()
	{
		printf("Base Test \n");
	}
	Widget* _arr;
private:

};

Base::Base()
{
	printf("Base Constructor \n");
	_arr = new Widget[10];
}

Base::~Base()
{
	printf("Base Destructor \n");
	delete[] _arr;
}

class Derived : public Base
{
public:
	Derived();
	virtual ~Derived();
	virtual void Test()
	{
		printf("Derived Test \n");
	}
	Widget* _arr2;
private:

};

Derived::Derived()
{
	printf("Derived Constructor \n");
	_arr2 = new Widget[10];
}

Derived::~Derived()
{
	printf("Derived Destructor \n");
	delete[] _arr2;
}

void Test1()
{
	MemoryPool::MemoryPool<Base> pool(100, false);
	Base* arr[100];

	PROFILE_INITIALIZE;

	for (unsigned int j = 0; j < 100; j++)
	{
		PROFILE_BEGIN(L"pool");
		for (unsigned int index = 0; index < 100; index++)
		{
			arr[index] = pool.Alloc();
		}

		for (unsigned int index = 0; index < 100; index++)
		{
			pool.Free(arr[index]);
		}
		PROFILE_END(L"pool");
	}

	for (unsigned int j = 0; j < 100; j++)
	{
		PROFILE_BEGIN(L"normal");
		for (unsigned int index = 0; index < 100; index++)
		{
			arr[index] = new Base;
		}

		for (unsigned int index = 0; index < 100; index++)
		{
			delete arr[index];
		}
		PROFILE_END(L"normal");
	}
	PROFILE_OUT;
}

void Test2()
{
	int number_of_blocks;
	int b_placement_new;
	int count = 0;
	while (1)
	{
		number_of_blocks = rand() % 2000;
		b_placement_new = rand() % 2;

		MemoryPool::MemoryPool<Base> pool(number_of_blocks, (bool)b_placement_new);
		Base** arr = new Base*[number_of_blocks];

		
		for (int index = 0; index < number_of_blocks; index++)
		{
			arr[index] = pool.Alloc();
		}

		for (int index = 0; index < number_of_blocks; index++)
		{
			pool.Free(arr[index]);
		}
		
		count++;
		printf("[number_of_blocks] : %d \n[b_placement_new] : %d\n[count] : %d \n", number_of_blocks, b_placement_new, count);
		Sleep(100);
		pool.~MemoryPool();
		delete[] arr;
		if (count == 1)
		{
			break;
		}
	}
}

void Test3()
{
	MemoryPool::MemoryPool<Base> pool(100, true);

	Base* arr[100];

	for (unsigned int j = 0; j < 100; j++)
	{
		for (unsigned int index = 0; index < 100; index++)
		{
			arr[index] = pool.Alloc();
		}

		for (unsigned int index = 0; index < 100; index++)
		{
			pool.Free(arr[index]);
		}
	}
}

void Test4(int a)
{
	printf("%d \n", a);
	Test4(a + 1);
}

int main(void)
{
	MemoryPool::MemoryPool<Base> pool(3, false);

	Base* p1 = pool.Alloc();
	Base* p2 = pool.Alloc();
	Base* p3 = pool.Alloc();
	Base* p4 = pool.Alloc();

	pool.Free(p1);
	pool.Free(p2);
	pool.Free(p3);
	pool.Free(p4);

	Base* p5 = pool.Alloc();
	Base* p6 = pool.Alloc();
	Base* p7 = pool.Alloc();

	pool.Free(p5);
	pool.Free(p6);
	pool.Free(p7);


 	return 0;
}