/* ***************************************
	"#define PROFILE_ON"으로 활성화
	
	ex)

	PROFILE_BEGIN("함수이름")
	함수();
	PROFILE_END("함수 이름")
	
**************************************** */


#include <stdio.h>
#include <time.h>
#include <windows.h>
#include "profiler.h"

/* ***************************************
	전역변수
**************************************** */
FILE* g_profiler_fp = nullptr;
Profiler::ProfileSlot g_profiler_slots[PROFILE_ARR_MAX];

void Profiler::ProfileInitialize()
{
	for (unsigned int i = 0; i < PROFILE_ARR_MAX; i++)
	{
		g_profiler_slots[i]._name[0] = '\0';
		g_profiler_slots[i]._min1 = 0xFFFFFFFF;
		g_profiler_slots[i]._min2 = 0xFFFFFFFF;
		g_profiler_slots[i]._max1 = 0;
		g_profiler_slots[i]._max2 = 0;
		g_profiler_slots[i]._start = 0;
		g_profiler_slots[i]._accumulated_call = 0;
		g_profiler_slots[i]._accumulated_tick = 0;
	}
}

void Profiler::ProfileBegin(wchar_t* name)
{
	LARGE_INTEGER start;
	QueryPerformanceCounter(&start);

	for (unsigned int index = 0; index < PROFILE_ARR_MAX; index++)
	{
		if (g_profiler_slots[index]._name[0] == '\0')
		{
			wcscpy_s(g_profiler_slots[index]._name, g_profiler_slots[index].NAME_LENGTH, name);
			g_profiler_slots[index]._start = start.QuadPart;

			break;
		}

		if (wcscmp(name, g_profiler_slots[index]._name) == 0)
		{
			/* ***************************************
				BEGIN을 두 번 했을 경우에
				오류 상황임을 알려준다.
			**************************************** */
			if (g_profiler_slots[index]._start != 0)
			{
				throw 1;
			}
			else
			{
				g_profiler_slots[index]._start = start.QuadPart;

				break;
			}
		}
	}

	if (GetAsyncKeyState(0x30) & 0x8001)
	{
		ProfileOutText();
	}
}

void Profiler::ProfileEnd(wchar_t* name)
{
	LARGE_INTEGER end;
	QueryPerformanceCounter(&end);

	for (unsigned int index = 0; index < PROFILE_ARR_MAX; index++)
	{
		/* ***************************************
			BEGIN 없이 END한 경우, 오류 표시
		**************************************** */
		if (g_profiler_slots[index]._name[0] == '\0')
		{
			throw 1;
		}

		if (g_profiler_slots[index]._name[0] != '\0' && wcscmp(g_profiler_slots[index]._name, name) == 0)
		{
			/* ***************************************
				END만 두 번한 경우, 오류 표시
			**************************************** */
			if (g_profiler_slots[index]._start == 0)
			{
				throw 1;
			}

			long long accumulated = end.QuadPart - g_profiler_slots[index]._start;
			g_profiler_slots[index]._accumulated_tick += accumulated;
			g_profiler_slots[index]._accumulated_call += 1;
			g_profiler_slots[index]._start = 0;

			/* ***************************************
				가장 작은 min, 두 번째로 작은 min을
				구함.
			**************************************** */
			if (accumulated < g_profiler_slots[index]._min1)
			{
				g_profiler_slots[index]._min2 = g_profiler_slots[index]._min1;
				g_profiler_slots[index]._min1 = accumulated;
			}

			if (accumulated > g_profiler_slots[index]._min1 && accumulated < g_profiler_slots[index]._min2)
			{
				g_profiler_slots[index]._min2 = accumulated;
			}

			/* ***************************************
				가장 큰 max, 두 번째로 큰 max를 구함.
			**************************************** */
			if (accumulated > g_profiler_slots[index]._max1)
			{
				g_profiler_slots[index]._max2 = g_profiler_slots[index]._max1;
				g_profiler_slots[index]._max1 = accumulated;
			}

			if (accumulated < g_profiler_slots[index]._max1 && accumulated > g_profiler_slots[index]._max2)
			{
				g_profiler_slots[index]._max2 = accumulated;
			}

			break;
		}
	}
}

void Profiler::ProfileOutText()
{
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	float micro_time = (float)1000000 / freq.QuadPart;

	wchar_t file_name[128];
	struct tm t;
	time_t current;

	time(&current);
	localtime_s(&t, &current);

	swprintf_s(file_name, 128, L"Profiling_%d%d%d_%d%d%d.txt", t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec);
	_wfopen_s(&g_profiler_fp, file_name, L"w,ccs=UTF-16LE");
	if (g_profiler_fp == NULL)
	{
		throw 1;
	}

	/* ***************************************
		포맷 정리	
	**************************************** */
	wchar_t title[128] = L"           Name  |     Average  |        Min   |        Max   |      Call         |\n";
	fwrite(title, sizeof(wchar_t), wcslen(title) + 1, g_profiler_fp);
	swprintf_s(title, L"----------------------------------------------------------------------------------\n");
	fwrite(title, sizeof(wchar_t), wcslen(title) + 1, g_profiler_fp);

	// min max 정리
	for (unsigned int i = 0; i < PROFILE_ARR_MAX; i++)
	{
		if (g_profiler_slots[i]._name[0] != '\0')
		{
			/* ***************************************
				함수가 한 번만 호출되어 min2와 max2가
				비어있는 경우
			**************************************** */
			if (g_profiler_slots[i]._max2 == 0)
			{
				g_profiler_slots[i]._max2 = g_profiler_slots[i]._max1;
			}

			if (g_profiler_slots[i]._min2 == 0xFFFFFFFF)
			{
				g_profiler_slots[i]._min2 = g_profiler_slots[i]._min1;
			}
		}
		else
		{
			break;
		}
	}

	wchar_t format[128] = L"%16s|%12.2f㎲|%12.2f㎲|%12.2f㎲|%19u|\n";
	wchar_t out[128];

	for (unsigned int index = 0; index < PROFILE_ARR_MAX; index++)
	{
		if (g_profiler_slots[index]._name[0] != '\0')
		{
			long long accumulated_tick;
			long long accumulated_call;

			/* ***************************************
				함수 호출이 4번 보다 크다면,
				max12, min12을 제외한 수치를 이용하여
				평균을 구한다.
			**************************************** */
			if (g_profiler_slots[index]._accumulated_call > 4)
			{
				accumulated_tick = g_profiler_slots[index]._accumulated_tick -
					g_profiler_slots[index]._max1 -
					g_profiler_slots[index]._max2 -
					g_profiler_slots[index]._min1 -
					g_profiler_slots[index]._min2;
				accumulated_call = g_profiler_slots[index]._accumulated_call - 4;
			}
			else
			{
				accumulated_tick = g_profiler_slots[index]._accumulated_tick;
				accumulated_call = g_profiler_slots[index]._accumulated_call;
			}

			swprintf_s(out, format, 
				g_profiler_slots[index]._name,
				(accumulated_tick * micro_time) / accumulated_call,
				g_profiler_slots[index]._min2 * micro_time,
				g_profiler_slots[index]._max2 * micro_time,
				g_profiler_slots[index]._accumulated_call);

			fwrite(out, sizeof(wchar_t), wcslen(out) + 1, g_profiler_fp);
		}
		else
		{
			break;
		}
	}

	fclose(g_profiler_fp);
}
